# cnatural

The zen of cnatural is to remove ALL redundancy from the C++ language.
Any piece of information should appear in only one location.
No syntax should exist which is unnecessary.

In particular this means:
- remove header files (as I first asked about on 200406-24 on comp.lang.c++.moderated, at https://groups.google.com/forum/#!search/charles.fox$20header$20files/comp.lang.c++.moderated/0OQXBYFbZhs/VPzXErmU80wJ !)
- one class per file convention. remove class names from inside their files, as their filenames already contain it
- remove superfluous semicolons, carriage returns are sufficient
- remove curley brackets, indendation is sufficient

It would be nice extend c natural with:
- proper parsing compiler to replace this hacky one
- replace "int x" type syntax with the more beautiful "x:int" type syntax
- gdb integration
- cmake integration
- vim syntax highlights and youcompleteme autocompletion integration
- any further redundancy removals

2020-05-12: This Python repo is now just here for historical interest. There is a newer and better implementation of CNatural by Matthew Murr and Alice Johnston here, based on flex/bison and extended with tool support for LSP, gdb and cmake:
https://gitlab.com/cnatural/cn
